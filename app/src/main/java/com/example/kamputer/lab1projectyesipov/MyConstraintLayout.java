package com.example.kamputer.lab1projectyesipov;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

public class MyConstraintLayout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_constraint_layout);

        ConstraintLayout layout = findViewById(R.id.mylayout);

        setContentView(layout);

        GridView mygrid = findViewById(R.id.mygridview);
        final Intent i = new Intent(this, GridClick.class);
        mygrid.setAdapter(new ImageAdapter(this));
        mygrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MyConstraintLayout.this, "" + position, Toast.LENGTH_SHORT).show();
                switch (position) {
                    case 0:
                       i.putExtra("iconName", "hurricane");
                       break;
                    case 1:
                        i.putExtra("iconName", "keys");
                        break;
                    case 2:
                        i.putExtra("iconName", "location");
                        break;
                    default:
                        i.putExtra("iconName", "3-5");
                        break;
                }
                startActivity(i);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyApplication.addActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.delActivity(this);
    }
}
