package com.example.kamputer.lab1projectyesipov;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class MyWebView extends AppCompatActivity {

    private WebView myWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        myWeb = findViewById(R.id.myweb);
        myWeb.getSettings().setJavaScriptEnabled(true);
        myWeb.loadUrl("https://www.google.com.ua/");
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyApplication.addActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.delActivity(this);
    }
}
