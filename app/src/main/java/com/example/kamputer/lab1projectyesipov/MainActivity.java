package com.example.kamputer.lab1projectyesipov;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.addActivity(this);
        Log.d("DEBUG", "onCreate");

        Spinner spinner = findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                String[] choose = getResources().getStringArray(R.array.myList);
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Ваш выбор: " + choose[selectedItemPosition], Toast.LENGTH_SHORT);
                toast.show();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("DEBUG", "onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("DEBUG", "onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("DEBUG", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("DEBUG", "onStop");
        stopService(new Intent(this, MyService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("DEBUG", "onDestroy");
        MyApplication.delActivity(this);
    }
}
