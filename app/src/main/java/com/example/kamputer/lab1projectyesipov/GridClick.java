package com.example.kamputer.lab1projectyesipov;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.widget.TextView;

public class GridClick extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_click);

        ConstraintLayout layout = findViewById(R.id.gridclick);

        TextView tv = new TextView(this);
        tv.setText("You choose a " + getIntent().getStringExtra("iconName") + " icon!");
        layout.addView(tv);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.delActivity(this);
    }
}
