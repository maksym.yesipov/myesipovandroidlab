package com.example.kamputer.lab1projectyesipov;

import android.app.Application;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MyApplication extends Application {
    public static List<AppCompatActivity> activity_list = new ArrayList<AppCompatActivity>();

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "Application started", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public static void addActivity(AppCompatActivity a){
        activity_list.add(a);
    }

    public static void delActivity(AppCompatActivity a){
        Integer index = activity_list.indexOf(a);
        if(index != -1){
            AppCompatActivity delAct = activity_list.get(index);
            delAct.finish();
        }
    }
    public static void delAllActivities(){
        for(AppCompatActivity act:activity_list){
            act.finish();
        }
        System.exit(0);
    }
}
