package com.example.kamputer.lab1projectyesipov;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.mapview.MapView;

public class MyMapVIew extends AppCompatActivity {

    private MapView mapview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapKitFactory.setApiKey("35003b25-b1b1-4d26-b08d-e025fb832ad2");
        MapKitFactory.initialize(this);

        setContentView(R.layout.activity_my_map_view);

        mapview = findViewById(R.id.mapview);
        mapview.getMap().move(
                new CameraPosition(new Point(55.751574, 37.573856), 11.0f, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0),
                null);

    }

    @Override
    protected void onStart() {
        super.onStart();
        MyApplication.addActivity(this);
        mapview.onStart();
        MapKitFactory.getInstance().onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.delActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapview.onStop();
        MapKitFactory.getInstance().onStop();
    }
}
