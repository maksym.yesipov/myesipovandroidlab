package com.example.kamputer.lab1projectyesipov;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class SecMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sec_main);
        Log.d("DEBUG", "Parent: onCreate");

        LinearLayout layout = findViewById(R.id.linearLayout);


        Button button = new Button(this);
        button.setText("Exit!");
        layout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.delAllActivities();
            }
        });
    }

    public void layoutClick(View view) {
        startActivity(new Intent(this, MainActivity.class));
        startService(new Intent(this, MyService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("DEBUG", "Parent: onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("DEBUG", "Parent: onStart");
        MyApplication.addActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("DEBUG", "Parent: onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("DEBUG", "Parent: onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("DEBUG", "Parent: onDestroy");
        MyApplication.delActivity(this);
    }

    public void webClick(View view) {
        startActivity(new Intent(this, MyWebView.class));
    }

    public void mapClick(View view) {
        startActivity(new Intent(this, MyMapVIew.class));
    }

    public void ConstraintLayout(View view) {
        startActivity(new Intent(this, MyConstraintLayout.class));
    }
}
